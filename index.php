<?php
/**
 * Created by PhpStorm.
 * User: tombar
 * Date: 15.04.2020
 * Time: 15:36
 */
session_start();
$loginStatus = 'init';

if (isset($_POST["login"])) {

    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    $password = filter_input(INPUT_POST, 'pwd');

    // only for demo !
    if ($email === 'guitar@tombar.de' && $password === 'PanzerNashornBaby') {
        $loginStatus = 'Ok';
        $_SESSION['qwerz'] = $password;
        header('Location: scp/uploadAudio.php');

    }
    else{

        $loginStatus = 'Error';
    }

}

//phpinfo();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tombar Upload Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <style>

        #imageBanner {

            width: 100%;
            height: auto;
            margin: auto;

        }

        .border-3 {
            border-width:3px !important;
        }

    </style>

</head>
<body>

<div class="container mt-3">

    <!--    FlashMessage Error-->
    <?php if ($loginStatus === 'Error') { ?>

        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Login Error</strong> wrong email or password !
        </div>

    <?php } ?>



    <div class="card">
        <div class="card-header bg-info text-white text-center">

            <h5>Tombar Audio UploadPage Login</h5>

        </div>

        <div class="card-body border border-3 border-info">

            <div id="banner" class="container border border-3 border-warning p-0">

                <img id="imageBanner" src="images/bannerGuitarShow.jpg" style="" alt="">

            </div>


            <div class="row">

                <div class="col-sm-4 offset-sm-4 mt-3">

                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" METHOD="post" class="needs-validation"  novalidate>

                        <div style="color:#17a2b2;font-weight:900;" class=" border border-3 border-info p-3">

                            <div class="form-group ">
                                <label  for="email">Email:</label>
                                <input type="email" class="form-control text-info" id="email" placeholder="Enter email" name="email">
                                <div class="invalid-feedback">Please enter a valid email address.</div>
                            </div>
                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" class="form-control text-info" id="pwd" placeholder="Enter password" name="pwd">
                                <div class="invalid-feedback">Please enter your password to continue.</div>
                            </div>

                            <button type="submit" class="btn btn-info " name="login">Login</button>

                        </div>
                    </form>

                </div>

            </div>

        </div>

        <!-- JavaScript for disabling form submissions if there are invalid fields -->
        <script>
            // Self-executing function
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    // Fetch all the forms we want to apply custom Bootstrap validation styles to
                    let forms = document.getElementsByClassName('needs-validation');
                    // Loop over them and prevent submission
                    let validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();
        </script>

</body>
</html>