<?php
session_start();
if (!isset($_SESSION['qwerz']) or $_SESSION['qwerz'] !== 'PanzerNashornBaby') {
    header("Location:../index.php");
}

$target_dir = "../videoMp4/";

// vorhandene videofiles aus dem videoOrdner einlesen
foreach (glob($target_dir . "*.*") as $videoFile) {


    $fileType = strtolower(pathinfo($videoFile, PATHINFO_EXTENSION));

    if($fileType === 'mp4')
    {
        // Path entfernen
        $videoFile = substr(strrchr($videoFile, '/'), 1);
        $videoFiles[] = $videoFile;
    }


}


?>

<head>

    <title>Tombar & Rabar Fileupload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">


    <style>
        video {
            width: 100%;
            height: auto;
        }

    </style>
</head>

<body>

<div class="container mt-3 ">

    <div class="mt-3 text-center">

        <video id="videoPlayer"   controls>
            Your browser does not support the audio element.
        </video>

    </div>


    <div class="mt-3">

        <div class="card">
            <div class="card-header bg-secondary text-white text-center">

                <h5>Video Table</h5>

            </div>

            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-hover ">
                        <thead>
                        <tr class="text-info">
                            <th>FILENAME</th>
                            <th>PLAY</th>
                            <th>DELETE</th>
                            <th>DOWNLOAD</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($videoFiles as $videoFile) { ?>

                            <tr>
                                <td class="text-secondary font-weight-bold"><?= $videoFile ?></td>
                                <td>
                                    <button class="btn btn-info"><i data-file="<?= $target_dir . $videoFile ?>"
                                                                    class="fas fa-play-circle"></i></button>

                                </td>
                                <td>

                                    <button class="btn btn-danger" data-file="<?= $target_dir . $videoFile ?>"><i
                                                class="fas fa-trash-alt"></i></button>

                                </td>
                                <td><a href="<?= $target_dir . $videoFile ?>" class="btn btn-success" role="button"
                                       download><i class="fas fa-download"></i></a></td>


                            </tr>

                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>

            <div class="card-footer bg-info text-white text-center">

                <a href="uploadAudio.php" class="btn btn-secondary" role="button" >Show Audio Page</a>


            </div>

        </div>


    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

<script>



    $(document).ready(function () {

        console.log("click Play");


        // Play / Pause audioFile in the table
        $('.fa-play-circle').on('click', function () {

            console.log("click Play");

            let className = '';

            let filename = $(this).attr('data-file');

            className = $(this).attr('class');

            let vid = document.getElementById("videoPlayer");

            if (className === 'fas fa-play-circle') {
                vid.src = filename;
                vid.play();
            }

            if (className === 'fas fa-stop-circle') {

                vid.pause();
            }

            $(this).toggleClass('fa-play-circle fa-stop-circle');

        });



    });

</script>


</body>
