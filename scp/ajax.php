<?php
/**
 * Created by PhpStorm.
 * User: tombar
 * Date: 13.04.2020
 * Time: 15:55
 */

set_time_limit(0);
ini_set('post_max_size', '100M');
ini_set('upload_max_filesize', '90M');

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);


$delete = 'error';
// Delete file
if (isset($_POST['filename'])) {

    if (unlink($_POST['filename'])) {
        $delete = "ok";

    }

    echo $delete;
    exit;
}

if (isset($_POST['checkFile'])) {

    // File upload configuration
    $targetDir = "../uploads/";
    $allowedTypes = array('mp3', 'wav');

    $fileName = $_POST['checkFile'];
    $targetFilePath = $targetDir . $fileName;

    // Check whether file type is valid
    $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
    if (!in_array($fileType, $allowedTypes, true)) {
        $upload = 'errFileType';
        echo $upload;
        exit;
    }


    if (file_exists($targetFilePath)) {
        $upload = 'errFileExists';
        echo $upload;
        exit;
    }

    $upload = 'fileOk';
    echo $upload;
    exit;

}


$upload = 'errFileUpload';

if (!empty($_FILES['file'])) {

    // File upload configuration
    $targetDir = "../uploads/";
    $allowedTypes = array('mp3', 'wav');

    $fileName = basename($_FILES['file']['name']);
    $targetFilePath = $targetDir . $fileName;


    $size = $_FILES['file']['size'];
    if ($size > 60000000) {
        $upload = 'errFileSize';
        echo $upload;
        exit;
    }

    // Upload file to the server
    if (move_uploaded_file($_FILES['file']['tmp_name'], $targetFilePath)) {

        $uploadMessage = "Last uploaded file : " . $fileName . "  " . date("d-m-Y h:i:sa");

        if (isset($_POST["emailNotification"])) {

            send_email($fileName);
        }

        // last upload speichern
        file_put_contents('../data/lastUpload.txt', $uploadMessage);

        $upload = 'Ok';
        echo $upload;
        exit;
    }

    // error
    echo $upload;
    exit;

}


function send_email($fileName)
{

    ob_start();
    ?>
    <div style="margin:40px">
        <h3>New Fileupload :</h3>
        <h4 style="color:orangered;font-weight: bold"><?= $fileName ?></h4>
        <p style="color:blue;font-weight: bold">Upload Time : <?= date("d-m-Y h:i:sa") ?></p>
    </div>
    <?php
    $messageHtml = ob_get_clean();


    $to = "th.barwanietz@freenet.de,ralfb.london@googlemail.com";
    $subject = "New Fileupload";
    $txt = $messageHtml;

    $header = "MIME-Version: 1.0\r\n";
    $header .= "Content-type: text/html; charset=utf-8\r\n";
    $header .= "From: guitar@tombar.de";

    mail($to, $subject, $txt, $header);

}