<?php
session_start();
// only for demo !
if(!isset($_SESSION['qwerz']) or $_SESSION['qwerz'] !== 'PanzerNashornBaby'){
    header("Location:../index.php");
}


$target_dir = "../uploads/";
$uploadOk = 1;
$fileUploadStatus = 'init';
$fileDeleteStatus = 'init';
$successMessage = '';
$errorMessage = '';

$uploadedFile = '';
$uploadMessage = '';
$audioFiles = array();
$sizeError = false;

// für die flashMessage
if (isset($_GET["deleteStatus"])) {

    $filename = $_GET["file"];
    if ($_GET["deleteStatus"] === 'Ok') {
        $fileDeleteStatus = 'Ok';
        $successMessage = "File : " . $filename . " deleted";
    } else {
        $fileDeleteStatus = 'Error';
        $errorMessage = "File: $filename   not deleted ";
    }

}

// für die flashMessage
if (isset($_GET["uploadStatus"])) {

    $filename = $_GET["file"];

    if ($_GET["uploadStatus"] === 'Ok') {
        $fileUploadStatus = 'Ok';
        $successMessage = "File : " . $filename . " uploaded";
        $uploadMessage = "Last uploaded file : " . $filename . " " . date("d-m-Y h:i:sa");
    } else if ($_GET["uploadStatus"] === 'errFileUpload'){
        $fileUploadStatus = 'Error';;
        $errorMessage = "File: $filename   not uploaded ";
    } else if ($_GET["uploadStatus"] === 'errFileExists') {
        $fileUploadStatus = 'Error';
        $errorMessage = "File: $filename   already exists ";
    } else if ($_GET["uploadStatus"] === 'errFileSize') {
        $fileUploadStatus = 'Error';
        $errorMessage = "File: $filename   filesize to big only 60M allowed";
    } else if ($_GET["uploadStatus"] === 'errFileType') {
        $fileUploadStatus = 'Error';
        $errorMessage = "File: $filename   wrong fileType only wav, mp3 allowed";
    }

}


// vorhandene audiofiles aus dem uploadOrdner einlesen
foreach (glob($target_dir . "*.*") as $audioFile) {


    $fileType = strtolower(pathinfo($audioFile, PATHINFO_EXTENSION));

   if($fileType === 'wav' or $fileType === 'mp3')
   {
       // Path entfernen
       $audioFile = substr(strrchr($audioFile, '/'), 1);
       $audioFiles[] = $audioFile;
   }


}

// last uploadDate from File
$uploadMessage = file_get_contents('../data/lastUpload.txt');

?>


<!DOCTYPE html>
<html lang="en">

<head>

    <title>Tombar & Rabar Fileupload</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <style>

          /*custom checkbox color selected*/
        .custom-checkbox .custom-control-input:checked~.custom-control-label::before{
            background-color:#17a2b8;
        }

        /*Browse-Button fileupload*/
        .custom-file-label::after{

            background-color:#17a2b8;
            color:white;
            font-weight: bold;
        }

    </style>




</head>

<body>


<!-- Modal -->
<div class="modal fade" id="deleteModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Delete Audio File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete-audio" data-file="">Delete</button>
            </div>
        </div>
    </div>
</div>


<div class="container mt-3 " >

<!--    FlashMessage Success-->
    <?php if ($fileUploadStatus === 'Ok' or $fileDeleteStatus === 'Ok') { ?>

        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Success!</strong> <?= $successMessage ?>
        </div>

    <?php } ?>

<!--    FlashMessage Error-->
    <?php if ($fileUploadStatus === 'Error' or $fileDeleteStatus === 'Error') { ?>

        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>Error</strong> Sorry an error occurs <?= $errorMessage ?>
        </div>

    <?php } ?>


    <div class="card">
        <div class="card-header bg-info text-white text-center">

            <h5>Upload Audio Files</h5>

        </div>

        <div class="card-body">

            <div class="row">
                <div class="col-sm-12">
                    <form id="uploadForm" enctype="multipart/form-data">
                        <div class="form-group">

                            <div class="input-group">

                                <div class="custom-file">

                                    <input type="file" class="custom-file-input " name="file" id="fileInput">


                                        <label class="custom-file-label " for="fileInput" data-browse="BROWSE" style="color:#17a2b2;font-weight:900; ">Choose file</label>


                                </div>
                            </div>

                            <div class="input-group mt-3 ">

                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="emailNotification" name="emailNotification">
                                    <label class="custom-control-label" for="emailNotification"><span style="color:#17a2b2;font-weight:900; ">Check for email notification</span></label>
                                </div>
                            </div>

                            <div class="input-group mt-3">

                                <button type="submit" name="submit" value="UPLOAD" class="btn btn-info">Submit</button>

                           </div>

                            <div class="progress mt-3">
                                <div class="progress-bar progress-bar-striped  progress-bar-animated bg-info" > </div>
                            </div>

                        </div>

                    </form>

                </div>

            </div>


        </div>
    </div>

    <div class="card-footer bg-info text-white text-center">

        <h6>File Format : &nbsp;&nbsp;.wav .mp3 &nbsp;&nbsp;max size 60M</h6>

    </div>

    <div class="mt-3 text-center">

        <audio id="audioPlayer" controls>
            Your browser does not support the audio element.
        </audio>

    </div>


    <div class="mt-3">

        <div class="card">
            <div class="card-header bg-secondary text-white text-center">

                <h5>Download Table</h5>

            </div>

            <div class="card-body">

                <div class="table-responsive">
                <table class="table table-hover ">
                    <thead>
                    <tr class="text-info">
                        <th>FILENAME</th>
                        <th>PLAY</th>
                        <th>DELETE</th>
                        <th>DOWNLOAD</th>

                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($audioFiles as $audioFile) { ?>

                        <tr>
                            <td class="text-secondary font-weight-bold"><?= $audioFile ?></td>
                            <td>
                                <button class="btn btn-info"><i data-file="<?= $target_dir . $audioFile ?>" class="fas fa-play-circle"></i></button>

                            </td>
                            <td>

                                <button class="btn btn-danger" data-file="<?= $target_dir . $audioFile ?>" ><i class="fas fa-trash-alt"></i></button>

                            </td>
                            <td><a href="<?= $target_dir . $audioFile ?>" class="btn btn-success" role="button" download><i class="fas fa-download"></i></a></td>


                        </tr>

                    <?php } ?>

                    </tbody>
                </table>
                </div>
            </div>

            <div class="card-footer bg-secondary text-white text-center">

                <h6><?= $uploadMessage ?></h6>

            </div>

        </div>


    </div>

    <div class="mt-3 mb-3">

        <div class="card">
            <div class="card-header bg-info text-white text-center">

                <a href="videoPage.php" class="btn btn-secondary" role="button" >Show Video Page</a>

            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

    <script>



        $(document).ready(function () {

            // Add the following code if you want the name of the file appear on select
            $(".custom-file-input").on("change", function () {
                let fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });


            // Button download table Delete
            $('.btn-danger').on('click', function () {


                let deleteFilename = $(this).attr('data-file');


                // bodytext setzen und Path entfernen
                $('.modal-body').html('<h4>Do you really want to delete ? <br>' +deleteFilename.split("/").pop()+'</h4>');

                // Das Data Attribut für den Delete-Button wird gesetzt ( das zu löschende Audio file )
                $('.delete-audio').attr('data-file',deleteFilename);

               // modal dialog anzeigen
                $('#deleteModalCenter').modal('show');

            });

            // Button modal delete
            $('.delete-audio').on('click', function () {

                let deleteAudioFile = $(this).attr('data-file');

                $.ajax({

                    type: 'POST',
                    url: 'ajax.php',
                    data: { filename: deleteAudioFile },
                    success: function(response,stat,xhr)
                    {
                        if(response === 'ok')
                        {
                            window.location.replace("uploadAudio.php?deleteStatus=Ok&file=" + deleteAudioFile);
                        }
                        else
                        {
                            window.location.replace("uploadAudio.php?deleteStatus=Error&file=" + deleteAudioFile);
                        }
                    }

                });

            });

            // Play / Pause audioFile in the table
            $('.fa-play-circle').on('click', function () {

                let className = '';

                let filename = $(this).attr('data-file');

                className = $(this).attr('class');

                let aid = document.getElementById("audioPlayer");

                if (className === 'fas fa-play-circle') {
                    aid.src = filename;
                    aid.play();
                }

                if (className === 'fas fa-stop-circle') {

                    aid.pause();
                }

                $(this).toggleClass('fa-play-circle fa-stop-circle');

            });


            // File upload via Ajax
            $("#uploadForm").on('submit', function(e){
                e.preventDefault();

                let formData = new FormData(this);
                //Upload filename
                let filename = formData.get('file').name;

                console.log(filename);
                // checkFile before upload
                $.ajax({

                    type: 'POST',
                    url: 'ajax.php',
                    data: {checkFile:filename},
                    success: function(response,stat,xhr)
                    {
                        //alert(response);

                        // response = fileOk
                        // errFileSize, errFileType, errFileExists, errFileUpload,
                        if(response !== 'fileOk')
                        {
                            window.location.replace("uploadAudio.php?uploadStatus=" + response + "&file=" + filename);
                        }
                        else
                        {
                            let ajaxCall = $.ajax({
                                xhr: function() {
                                    let xhr = new window.XMLHttpRequest();
                                    xhr.upload.addEventListener("progress", function(evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = ((evt.loaded / evt.total) * 100);
                                            $(".progress-bar").width(percentComplete + '%');
                                            $(".progress-bar").html(parseInt(percentComplete) +'%');
                                        }
                                    }, false);
                                    return xhr;
                                },
                                type: 'POST',
                                url: 'ajax.php',
                             //   data: new FormData(this),
                                data: formData,
                                dataType: 'text',
                                contentType: false,
                                cache: false,
                                processData:false,
                                beforeSend: function(){
                                    $(".progress-bar").width('0%');
                                },
                                error:function(){
                                    $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
                                },

                                success: function(response,stat,xhr){

                                    if(stat === 'success'){

                                        // response = Ok, errFileSize, errFileUpload,

                                        window.location.replace("uploadAudio.php?uploadStatus=" + response + "&file=" + filename);


                                    }
                                }
                            });


                        }
                    }
                });
            });

        })

    </script>


</body>
</html>