# README #

## This is a demo application.
* * *
### What is this repository for? ###

* It combines different web technologies by using the example of an audio file upload
  via Ajax.
* For the file upload php 7.4 is used on the server.
  The Ajax calls are made with the jQuery Framework.
  The frontend is responsive and implemented with Bootstrap 4.
  Wave and mp3 audio files can be uploaded, downloaded, deleted and played.
  The audio files are played using the HTML5 audio tag
  with the associated javascript API.
  
  ![alternativetext](images/frontend.png)
  
